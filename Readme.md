# Jibble Assessment

This is a simple console application built with nodejs.

The application enables you to get and manage people details through an external API

## Getting Started
1. Clone the repo to a local directory on your system and open the folder in your terminal
2. Run the command `npm install` to install all dependencie
3. Use `npm start` to run the application
4. On startup, the application displays a list of options. Pick one to proceed