const { exit } = require('process');
const clear = require('clear');
const init = require('./lib/inquirer/init');
const chalk = require('chalk');
const { initOptions } = require('./config/userOptions');

const {
    fetchPeople,
    filterPeople,
    getPerson,
    addPerson,
    editPerson,
    deletePerson
} = require('./service/userService');

const run = async () => {
    clear();

    const {option, error} = await init.askQuestions();

    if (error) {
        console.log(chalk.yellow(error));
        exit();
    }

    switch (option) {
        case initOptions.LIST:
            fetchPeople();
            break;
        case initOptions.SEARCH:
            filterPeople();
            break;
        case initOptions.DETAILS:
            getPerson();
            break;
        case initOptions.CREATE:
            addPerson();
            break;
        case initOptions.EDIT:
            editPerson();
            break;
        case initOptions.DELETE:
            deletePerson();
            break;
        default:
            console.log(chalk.yellow('Unknown option selected'));
            break;
    }
};

run();