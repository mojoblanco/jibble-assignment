const { askForFilter } = require('../lib/inquirer/searchUser');
const { askForUsername } = require('../lib/inquirer/getUser');
const { askForCreateDetails } = require('../lib/inquirer/addUser');
const { askForUpdateDetails } = require('../lib/inquirer/editUser');
const { askForDelete } = require('../lib/inquirer/deleteUser');
const { displaySingleOutput, displayTableOutput, apiRequest } = require('../helpers/utils');
const { async } = require('rxjs');
const chalk = require('chalk');


module.exports = {
    fetchPeople: async () => {
        const { data, error } = await apiRequest('get', 'People');

        if (error) {
            console.log(`Unable to fetch people: ${error}`);
            return;
        }

        displayTableOutput(data);
    },
    filterPeople: async () => {
        const { option, searchValue, error: filterError } = await askForFilter();

        if (filterError) {
            console.log(`An error occured: ${error}`);
            return;
        }

        const { data, error } = await apiRequest('get', `People?$filter=${option} eq '${searchValue}'`);

        if (error) {
            console.log(`Unable to search for people: ${error}`);
            return;
        }

        displayTableOutput(data);
    },
    getPerson: async () => {
        const { username, error: askError } = await askForUsername();

        if (askError) {
            console.log(`An error occured: ${error}`);
            return;
        }

        const { data, error } = await apiRequest('get', `People('${username}')`);

        if (error) {
            console.log(`Unable to get person details: ${error}`);
            return;
        }

        displaySingleOutput(data);
    },
    addPerson: async () => {
        const { username, firstName, lastName, gender, error: askError } = await askForCreateDetails();

        if (askError) {
            console.log(`An error occured: ${error}`);
            return;
        }

        const payload = {
            UserName: username,
            FirstName: firstName,
            LastName: lastName,
            Gender: gender
        };
        const { error } = await apiRequest('post', `People`, payload);

        if (error) {
            console.log(`Unable to create person details: ${error}`);
            return;
        }

        console.log(chalk.green('Person details created successfully'));
    },
    editPerson: async () => {
        const response = await askForUsername();

        if (response.error) {
            console.log(`An error occured: ${response.error}`);
            return;
        }

        const person = await apiRequest('get', `People('${response.username}')`);

        if (person.error) {
            console.log(`An error occured: ${person.error}`);
            return;
        }

        if (!person.data) {
            console.log(`Person details was not found`);
            return;
        }

        const { firstName, lastName, gender, error: updateError } = await askForUpdateDetails(person.data);

        if (updateError) {
            console.log(`An error occured: ${error}`);
            return;
        }

        const payload = {
            FirstName: firstName,
            LastName: lastName,
            Gender: gender
        };
        const { error } = await apiRequest('post', `People('${response.username}')`, payload);

        if (error) {
            console.log(`Unable to update person details: ${error}`);
            return;
        }

        console.log(chalk.green('Person details updated successfully'));
    },
    deletePerson: async () => {
        const { username, confirm, error: askError } = await askForDelete();

        if (askError) {
            console.log(`An error occured: ${error}`);
            return;
        }

        if (!confirm) {
            console.log(`Operation canceled`);
            return;
        }

        const { error } = await apiRequest('delete', `People('${username}')`);

        if (error) {
            console.log(`Unable to delete person details: ${error}`);
            return;
        }

        console.log(chalk.green('Person details deleted successfully'));
    },
}
