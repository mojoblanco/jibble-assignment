module.exports = {
    initOptions: {
        LIST: 'list',
        SEARCH: 'search',
        DETAILS: 'details',
        CREATE: 'create',
        EDIT: 'edit',
        DELETE: 'delete'
    },
    filterOptions: {
        USERNAME: 'UserName',
        LAST_NAME: 'FirstName',
        FIRST_NAME: 'LastName',
        GENDER: 'Gender',
    }
}
