const inquirer = require('inquirer');

module.exports = {
    askForCreateDetails: async () => {
        try {
            const answers = await inquirer.prompt([
                {
                    type: 'input',
                    name: 'username',
                    message: 'Enter the username:',
                    validate: function (value) {
                        if (!value.length)
                            return 'Please enter a value';
                        return true;
                    }
                },
                {
                    type: 'input',
                    name: 'firstName',
                    message: 'Enter the first name:',
                    validate: function (value) {
                        if (!value.length)
                            return 'Please enter a value';
                        return true;
                    }
                },
                {
                    type: 'input',
                    name: 'lastName',
                    message: 'Enter the last name:',
                    validate: function (value) {
                        if (!value.length)
                            return 'Please enter a value';
                        return true;
                    }
                },
                {
                    type: 'input',
                    name: 'gender',
                    message: 'Enter the gender:',
                    validate: function (value) {
                        if (!value.length)
                            return 'Please enter a value';

                        if (!['male', 'female'].includes(value.toLowerCase()))
                            return 'Value must be either Male or Female';

                        return true;
                    }
                },
            ]);

            return answers;
        } catch (error) {
            return { username: null, error: 'Something went wrong' };
        }
    },

};