const inquirer = require('inquirer');
const { initOptions } = require('../../config/userOptions');

module.exports = {
    askQuestions: async () => {
        try {
            const answers = await inquirer.prompt([
                {
                    type: 'rawlist',
                    name: 'option',
                    message: 'What would you like to do?',
                    choices: [
                        { value: initOptions.LIST, name: 'List people' },
                        { value: initOptions.SEARCH, name: 'Search/Filter people' },
                        { value: initOptions.DETAILS, name: 'Show details of a specific person' },
                        { value: initOptions.CREATE, name: 'Create a new person' },
                        { value: initOptions.EDIT, name: 'Edit a person' },
                        { value: initOptions.DELETE, name: 'Delete a person' },
                    ]
                }
            ]);

            return answers;
        } catch (error) {
            return { option: null, error: 'Something went wrong' };
        }
    },

};