const inquirer = require('inquirer');

module.exports = {
    askForUsername: async () => {
        try {
            const answers = await inquirer.prompt([
                {
                    type: 'input',
                    name: 'username',
                    message: 'Enter the username:',
                    validate: function (value) {
                        if (!value.length)
                            return 'Please enter a value';
                        return true;
                    }
                }
            ]);

            return answers;
        } catch (error) {
            return { username: null, error: 'Something went wrong' };
        }
    },

};