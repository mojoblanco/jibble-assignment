const inquirer = require('inquirer');
const { filterOptions } = require('../../config/userOptions');

module.exports = {
    askForFilter: async () => {
        try {
            const answers = await inquirer.prompt([
                {
                    type: 'rawlist',
                    name: 'option',
                    message: 'What would you like to filter by?',
                    choices: [
                        { value: filterOptions.USERNAME, name: 'Username' },
                        { value: filterOptions.FIRST_NAME, name: 'First Name' },
                        { value: filterOptions.LAST_NAME, name: 'Last Name' },
                        { value: filterOptions.GENDER, name: 'Gender' },
                    ]
                },
                {
                    type: 'input',
                    name: 'searchValue',
                    message: 'Enter your search value:',
                    validate: function (value) {
                        if (!value.length)
                            return 'Please enter a value';
                        return true;
                    }
                },
            ]);

            return answers;
        } catch (error) {
            return { option: null, error: 'Something went wrong' };
        }
    },

};