const inquirer = require('inquirer');

module.exports = {
    askForDelete: async () => {
        try {
            const answers = await inquirer.prompt([
                {
                    type: 'input',
                    name: 'username',
                    message: 'Enter the username:',
                    validate: function (value) {
                        if (!value.length)
                            return 'Please enter a value';
                        return true;
                    }
                },
                {
                    type: 'confirm',
                    name: 'confirm',
                    message: 'Are you sure?',
                    default: false,
                }
            ]);

            return answers;
        } catch (error) {
            return { username: null, error: 'Something went wrong' };
        }
    },

};