const { table } = require('table');
const axios = require('axios');
const _ = require('lodash');
const CLI = require('clui');
const Spinner = CLI.Spinner;

const instance = axios.create({
    baseURL: 'https://services.odata.org/TripPinRESTierService/',
});

module.exports = {
    displaySingleOutput: (data) => {
        if (!data) {
            console.log('\n');
            console.log('No item found.');

            return;
        }

        const output = _.pick(data, ['UserName', 'FirstName', 'LastName', 'Gender']);

        console.log('\n');
        console.table(output);
    },
    displayTableOutput: (data) => {
        if (!data.value.length) {
            console.log('\n');
            console.log('No records found.');

            return;
        }

        let output = data.value.map(x => {
            return [
                x.UserName,
                x.FirstName,
                x.LastName,
                x.Gender,
            ]
        });
        output.unshift(['USERNAME', 'FIRST NAME', 'LAST NAME', 'GENDER']);

        const tableData = table(output, {});

        console.log('\n');
        console.log(tableData);
    },
    apiRequest: async (method, path, body = null) => {
        let response = {};
        let loader = new Spinner('Please wait...');
        loader.start();

        try {
            const { data } = await instance({
                method,
                url: path,
                data: body
            });

            response =  { data, error: null };
        } catch (error) {
            response = { data: null, error: error.message };
        }

        loader.stop();

        return response;
    }
}
